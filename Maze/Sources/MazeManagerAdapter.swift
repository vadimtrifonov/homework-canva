import TakeHomeTask
import MazeKit
import os.log

fileprivate typealias JSONObject = [String: Any]

fileprivate protocol MazeRoomJSONDecodable {
    
    init?(json: JSONObject, point: MazePoint)
}

fileprivate enum MazeManagerError: LocalizedError {
    case noData
    case invalidJSON
    
    var errorDescription: String? {
        switch self {
        case .noData:
            return NSLocalizedString("No data", comment: "")
        case .invalidJSON:
            return NSLocalizedString("Invalid JSON", comment: "")
        }
    }
}

final public class MazeManagerAdaper: MazeRepositoryProtocol {
    
    private static let log = OSLog(subsystem: String(describing: MazeManagerAdaper.self), category: "Repository")
    
    private typealias MazeManagerResponse = (data: Data?, error: Error?)
    
    private let mazeManager: MazeManager
    private lazy var operationQueue = OperationQueue()
    private lazy var completionQueue: DispatchQueue = DispatchQueue(label: "MazeManagerAdaper.completionQueue", attributes: .concurrent)
    
    init(mazeManager: MazeManager) {
        self.mazeManager = mazeManager
    }
    
    public func fetchStartingRoomIdentifier(pinnedTo point: MazePoint, completion: @escaping (Result<MazeRoomIdentifier>) -> Void) {
        let operation = AsyncBlockOperation { [weak self] operation in
            self?.mazeManager.fetchStartRoom { response in
                self?.completionQueue.async {
                    os_log("STARTING: fetched", log: MazeManagerAdaper.log)
                    
                    defer {
                        operation.finish()
                    }

                    guard let _self = self else {
                        os_log("STARTING: no self", log: MazeManagerAdaper.log)
                        return
                    }
                    
                    do {
                        let identifier: MazeRoomIdentifier = try _self.deserialize(response: response, point: point)
                        os_log("STARTING: deserialized", log: MazeManagerAdaper.log)
                        
                        if !operation.isCancelled {
                            completion(.success(identifier))
                            return
                        }
                        else {
                            os_log("FETCH: cancelled", log: MazeManagerAdaper.log)
                        }
                    }
                    catch {
                        os_log("STARTING: failed", log: MazeManagerAdaper.log)
                        
                        if !operation.isCancelled {
                            completion(.failure(error))
                            return
                        }
                        else {
                            os_log("FETCH: cancelled", log: MazeManagerAdaper.log)
                        }
                    }
                }
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
    public func fetchRoom(withIdentifier identifier: MazeRoomIdentifier, completion: @escaping (Result<MazeRoom>) -> Void) {
        let operation = AsyncBlockOperation { [weak self] operation in
            self?.mazeManager.fetchRoom(withIdentifier: identifier.id) { response in
                self?.completionQueue.async {
                    os_log("FETCH: fetched", log: MazeManagerAdaper.log)
                    
                    defer {
                        operation.finish()
                    }
                    
                    guard let _self = self else {
                        os_log("FETCH: no self", log: MazeManagerAdaper.log)
                        return
                    }
                    
                    do {
                        let room: MazeRoom = try _self.deserialize(response: response, point: identifier.point)
                        os_log("FETCH: deserialized", log: MazeManagerAdaper.log)
                        
                        if !operation.isCancelled {
                            completion(.success(room))
                        }
                        else {
                            os_log("FETCH: cancelled", log: MazeManagerAdaper.log)
                        }
                    }
                    catch {
                        os_log("FETCH: failed", log: MazeManagerAdaper.log)
                        
                        if !operation.isCancelled {
                            completion(.failure(error))
                        }
                        else {
                            os_log("FETCH: cancelled", log: MazeManagerAdaper.log)
                        }
                    }
                }
            }
        }
        
        operationQueue.addOperation(operation)
    }

    public func unlockRoomIdentifier(withLock lock: MazeRoomLock, completion: @escaping (MazeRoomIdentifier) -> Void) {
        let operation = BlockOperation()
        operation.addExecutionBlock { [weak self, weak operation] in
            guard
                let _self = self,
                let operation = operation else
            {
                os_log("UNLOCK: no self", log: MazeManagerAdaper.log)
                return
            }
            
            let id = _self.mazeManager.unlockRoom(withLock: lock.lock)
            os_log("UNLOCK: unlocked", log: MazeManagerAdaper.log)
            
            if !operation.isCancelled {
                _self.completionQueue.async {
                    completion(MazeRoomIdentifier(id: id, point: lock.point))
                }
            }
            else {
                os_log("UNLOCK: cancelled", log: MazeManagerAdaper.log)
                return
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
    public func unlockRoomIdentifier(withLock lock: MazeRoomLock) -> MazeRoomIdentifier {
        let id = mazeManager.unlockRoom(withLock: lock.lock)
        return MazeRoomIdentifier(id: id, point: lock.point)
    }
    
    public func cancel() {
        os_log("cancel all", log: MazeManagerAdaper.log)
        operationQueue.cancelAllOperations()
    }
    
    private func deserialize<T: MazeRoomJSONDecodable>(response: MazeManagerResponse, point: MazePoint) throws -> T {
        if let error = response.error {
            throw error
        }
        
        guard let data = response.data else {
            throw MazeManagerError.noData
        }
        
        guard
            let json = try JSONSerialization.jsonObject(with: data) as? JSONObject,
            let value = T(json: json, point: point)
        else {
            throw MazeManagerError.invalidJSON
        }

        return value
    }
    
    deinit {
        os_log("deinit", log: MazeManagerAdaper.log)
    }
}

extension MazeRoom: MazeRoomJSONDecodable {

    fileprivate init?(json: JSONObject, point: MazePoint) {
        guard
            let id = json["id"] as? String,
            let tileURLString = json["tileUrl"] as? String,
            let tileURL = URL(string: tileURLString),
            let rooms = json["rooms"] as? JSONObject
        else {
            return nil
        }
        
        self.init(
            id: id,
            point: point,
            tileURL: tileURL,
            north: MazeRoom.makeUnvisitedMazeRoom(rooms: rooms, direction: "north", point: point.north),
            east: MazeRoom.makeUnvisitedMazeRoom(rooms: rooms, direction: "east", point: point.east),
            south: MazeRoom.makeUnvisitedMazeRoom(rooms: rooms, direction: "south", point: point.south),
            west: MazeRoom.makeUnvisitedMazeRoom(rooms: rooms, direction: "west", point: point.west)
        )
    }
    
    private static func makeUnvisitedMazeRoom(rooms: JSONObject, direction: String, point: MazePoint) -> UnvisitedMazeRoom? {
        guard let json = rooms[direction] as? JSONObject else {
            return nil
        }
        return UnvisitedMazeRoom(json: json, point: point)
    }
}

extension UnvisitedMazeRoom: MazeRoomJSONDecodable {
    
    fileprivate init?(json: JSONObject, point: MazePoint) {
        if let identifier = MazeRoomIdentifier(json: json, point: point) {
            self = .unlocked(identifier)
        }
        else if let lock = MazeRoomLock(json: json, point: point) {
            self = .locked(lock)
        }
        else {
            return nil
        }
    }
}

extension MazeRoomLock: MazeRoomJSONDecodable {
    
    fileprivate init?(json: JSONObject, point: MazePoint) {
        guard let lock = json["lock"] as? String else {
            return nil
        }
        self.init(lock: lock, point: point)
    }
}

extension MazeRoomIdentifier: MazeRoomJSONDecodable {
    
    fileprivate init?(json: JSONObject, point: MazePoint) {
        guard let id = json["room"] as? String ?? json["id"] as? String else {
            return nil
        }
        self.init(id: id, point: point)
    }
}
