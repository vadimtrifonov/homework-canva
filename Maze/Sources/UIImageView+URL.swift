import UIKit
import AlamofireImage

extension UIImageView {
    
    func setImage(withURL url: URL) {
        af_setImage(withURL: url)
    }
}
