import UIKit
import TakeHomeTask
import MazeKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        guard let viewController = window?.rootViewController as? MazeViewPresenting else {
            fatalError("Unexpected view controller type")
        }
        viewController.presenter = makeMazeViewPresenter()
        
        return true
    }
    
    func makeMazeViewPresenter() -> MazeViewPresenter {
        let mazeManager = MazeManager()
        let mazeRepository = MazeManagerAdaper(mazeManager: mazeManager)
        let mazeInteractor = MazeInteractor(mazeRepository: mazeRepository)
        let mazeViewPresenter = MazeViewPresenter(mazeInteractor: mazeInteractor)
        
        return mazeViewPresenter
    }
}

