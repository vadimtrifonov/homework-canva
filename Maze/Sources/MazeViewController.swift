import UIKit
import os.log

final class MazeViewController: UIViewController, MazeViewPresenting {
    
    private static let log = OSLog(subsystem: String(describing: MazeViewController.self), category: "Controller")
    
    var presenter: MazeViewPresenter!
    
    @IBOutlet private weak var mazeBoard: MazeBoardView!
    @IBOutlet private weak var generateButton: UIButton!
    
    @IBAction func generate(_ sender: UIButton) {
        os_log("generate", log: MazeViewController.log)
        mazeBoard.removeAllTiles()
        generateButton.isEnabled = false
        
        let update: MazeViewPresenter.UpdateHandler = { [weak self] x, y, tileURL in
            let tile = UIImageView()
            tile.setImage(withURL: tileURL)
            os_log("update", log: MazeViewController.log)
            self?.mazeBoard.addTile(tile, x: x, y: y)
        }
        
        let completion: MazeViewPresenter.CompletionHandler = { [weak self] result in
            self?.generateButton.isEnabled = true
            
            switch result {
            case .finished(let message):
                self?.show(UIAlertController(message: message), sender: self)
            case .cancelled:
                os_log("cancelled", log: MazeViewController.log)
                self?.mazeBoard.removeAllTiles()
            case .failed(let error):
                self?.show(UIAlertController(error: error), sender: self)
            }
        }
        
        self.presenter.generateMazeFrom(x: mazeBoard.midpoint.x, y: mazeBoard.midpoint.y, update: update, completion: completion)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        os_log("cancel", log: MazeViewController.log)
        presenter.cancelMazeGeneration()
    }
    
    @IBAction func reset(_ sender: UIButton) {
        os_log("reset", log: MazeViewController.log)
        presenter = (UIApplication.shared.delegate as! AppDelegate).makeMazeViewPresenter()
        mazeBoard.removeAllTiles()
        generateButton.isEnabled = true
    }
}
