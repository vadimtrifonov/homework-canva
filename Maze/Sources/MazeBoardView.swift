import UIKit

final class MazeBoardView: UIScrollView, UIScrollViewDelegate {

    private static let tileSize = CGSize(width: 96 / UIScreen.main.scale, height: 96 / UIScreen.main.scale)
    
    private let tilesView = UIView()
    
    var midpoint: (x: Int, y: Int) {
        return (x: Int(center.x / MazeBoardView.tileSize.width), y: Int(center.y / MazeBoardView.tileSize.height))
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        resetTilesView()
        addSubview(tilesView)
        self.delegate = self
    }
    
    private func resetTilesView() {
        tilesView.frame = CGRect(origin: CGPoint.zero, size: frame.size)
        tilesView.bounds = tilesView.frame
    }

    func addTile(_ tile: UIImageView, x: Int, y: Int) {
        zoomScale = 1
        
        let tileOrigin = CGPoint(x: CGFloat(x) * MazeBoardView.tileSize.width, y: CGFloat(y) * MazeBoardView.tileSize.height)
        tile.frame = CGRect(origin: tileOrigin, size: MazeBoardView.tileSize)
        
        tilesView.addSubview(tile)
        
        let bounds = tilesView.bounds.union(tile.frame)
        tilesView.frame.size = bounds.size // Avoid changing the origin of the frame (because of the view's anchor point) when changing bounds
        tilesView.bounds = bounds
        
        contentSize = tilesView.bounds.size

        minimumZoomScale = min(frame.width / tilesView.bounds.width, frame.height / tilesView.bounds.height)
        zoomScale = minimumZoomScale
    }
    
    func removeAllTiles() {
        tilesView.subviews.forEach({ $0.removeFromSuperview() })
        resetTilesView()
    }
    
    // MARK: - UIScrollViewDelegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return tilesView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let offsetX = max(0, (bounds.width - contentSize.width) / 2)
        let offsetY = max(0, (bounds.height - contentSize.height) / 2)
        tilesView.frame.origin = CGPoint(x: offsetX, y: offsetY)
    }
}
