import Foundation
import MazeKit
import os.log

protocol MazeViewPresenting: class {
    
    var presenter: MazeViewPresenter! { get set }
}

final class MazeViewPresenter {
    
    private static let log = OSLog(subsystem: String(describing: MazeViewPresenter.self), category: "Presenter")
    
    enum CompletionResult {
        case finished(Message)
        case cancelled
        case failed(Error)
    }
    
    typealias Message = String
    typealias UpdateHandler = ((x: Int, y: Int, tileURL: URL)) -> Void
    typealias CompletionHandler = (CompletionResult) -> Void
    
    private let mazeInteractor: MazeInteractorProtocol
    
    init(mazeInteractor: MazeInteractorProtocol) {
        self.mazeInteractor = mazeInteractor
    }
    
    func generateMazeFrom(x: Int, y: Int, update: @escaping UpdateHandler, completion: @escaping CompletionHandler) {
        let startTime = CFAbsoluteTimeGetCurrent()
        
        let update: MazeInteractorProtocol.UpdateHandler = { point, tileURL in
            DispatchQueue.main.async {
                update((x: point.x, y: point.y, tileURL: tileURL))
            }
        }
        
        let completion: MazeInteractorProtocol.CompletionHandler = { result in
            DispatchQueue.main.async {
                switch result {
                case .finished:
                    let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
                    let message = String(format: NSLocalizedString("Maze was generated in %.2f seconds", comment: ""), timeElapsed)
                    completion(.finished(message))
                    
                case .cancelled:
                    completion(.cancelled)
                    
                case .failed(let error):
                    completion(.failed(error))
                }
            }
        }

        self.mazeInteractor.generate(fromPoint: MazePoint(x: x, y: y), update: update, completion: completion)
    }
    
    func cancelMazeGeneration() {
        self.mazeInteractor.cancel()
    }
    
    deinit {
        os_log("deinit", log: MazeViewPresenter.log)
    }
}
