import UIKit

extension UIAlertController {
    
    convenience init(error: Error) {
        self.init(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
        self.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
    }
    
    convenience init(message: String) {
        self.init(title: NSLocalizedString(message, comment: ""), message: nil, preferredStyle: .alert)
        self.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
    }
}
