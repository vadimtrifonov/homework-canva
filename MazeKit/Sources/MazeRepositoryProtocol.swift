import Foundation

public protocol MazeRepositoryProtocol {
    
    func fetchStartingRoomIdentifier(pinnedTo point: MazePoint, completion: @escaping (Result<MazeRoomIdentifier>) -> Void)
    
    func fetchRoom(withIdentifier identifier: MazeRoomIdentifier, completion: @escaping (Result<MazeRoom>) -> Void)
    
    func unlockRoomIdentifier(withLock lock: MazeRoomLock, completion: @escaping (MazeRoomIdentifier) -> Void)
    
    func unlockRoomIdentifier(withLock lock: MazeRoomLock) -> MazeRoomIdentifier
    
    func cancel()
}
