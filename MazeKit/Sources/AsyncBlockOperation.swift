import Foundation

public class AsyncBlockOperation: Operation {
    
    @objc enum State: Int {
        case ready
        case executing
        case finished
    }
    
    class func keyPathsForValuesAffectingIsExecuting() -> Set<NSObject> {
        return [#keyPath(state) as NSObject]
    }
    
    class func keyPathsForValuesAffectingIsFinished() -> Set<NSObject> {
        return [#keyPath(state) as NSObject]
    }
    
    public typealias AsyncBlock = (AsyncBlockOperation) -> Void
    
    private let block: AsyncBlock
    
    private let accessQueue = DispatchQueue(label: "AsyncBlockOperation.accessQueue", attributes: .concurrent)
    
    private var _state: State = .ready
    var state: State {
        get {
            return accessQueue.sync {
                return _state
            }
        }
        set {
            willChangeValue(forKey: #keyPath(state))
            
            accessQueue.async(flags: .barrier) {
                self._state = newValue
            }
            
            didChangeValue(forKey: #keyPath(state))
        }
    }
    
    override public var isExecuting: Bool {
        return state == .executing
    }
    
    override public var isFinished: Bool {
        return state == .finished
    }
    
    override public var isAsynchronous: Bool {
        return true
    }
    
    public init(block: @escaping AsyncBlock) {
        self.block = block
        super.init()
    }
    
    override public func start() {
        super.start()
        
        guard !isCancelled else {
            finish()
            return
        }
        
        state = .executing
        block(self)
    }
    
    public func finish() {
        self.state = .finished
    }
}
