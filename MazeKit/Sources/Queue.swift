import Foundation

/// A basic queue with average O(1) dequeue time and O(1) enqueue time
internal struct Queue<Element>: ExpressibleByArrayLiteral {
    private var array = [Element?]()
    private var head = 0
    
    internal var count: Int {
        return array.count - head
    }
    
    internal var isEmpty: Bool {
        return count == 0
    }
    
    internal init(arrayLiteral elements: Element...) {
        self.array = elements
    }
    
    internal mutating func enqueue(_ element: Element) {
        array.append(element)
    }
    
    internal mutating func enqueue<C: Collection>(contentsOf newElements: C) where C.Iterator.Element == Element {
        newElements.forEach({ array.append($0) })
    }
    
    internal mutating func dequeue() -> Element? {
        guard head < array.count, let element = array[head] else {
            return nil
        }
        
        array[head] = nil
        head += 1
        
        let ratio = Double(head) / Double(array.count)
        if array.count > 100 && ratio > 0.25 {
            array.removeFirst(head)
            head = 0
        }
        
        return element
    }
    
    internal mutating func removeAll(keepingCapacity: Bool) {
        array.removeAll(keepingCapacity: keepingCapacity)
        head = 0
    }
}
