
public struct MazePoint {
    public let x: Int
    public let y: Int
    
    public init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }

    public var north: MazePoint {
        return MazePoint(x: self.x, y: self.y + 1)
    }
    
    public var east: MazePoint {
        return MazePoint(x: self.x + 1, y: self.y)
    }
    
    public var south: MazePoint {
        return MazePoint(x: self.x, y: self.y - 1)
    }
    
    public var west: MazePoint {
        return MazePoint(x: self.x - 1, y: self.y)
    }
}
