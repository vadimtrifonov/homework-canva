import Foundation

public enum MazeInteractorCompletionResult {
    case finished
    case cancelled
    case failed(Error)
}

public protocol MazeInteractorProtocol {
    
    typealias UpdateHandler = ((point: MazePoint, tileURL: URL)) -> Void
    typealias CompletionHandler = (MazeInteractorCompletionResult) -> Void
    
    init(mazeRepository: MazeRepositoryProtocol)
    
    func generate(fromPoint: MazePoint, update: @escaping UpdateHandler, completion: @escaping CompletionHandler)
   
    func cancel()
}
