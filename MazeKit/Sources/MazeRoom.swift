import Foundation

public struct MazeRoomLock {
    
    public let lock: String
    public let point: MazePoint
    
    public init(lock: String, point: MazePoint) {
        self.lock = lock
        self.point = point
    }
}

public struct MazeRoomIdentifier {
    
    public let id: String
    public let point: MazePoint
    
    public init(id: String, point: MazePoint) {
        self.id = id
        self.point = point
    }
}

public enum UnvisitedMazeRoom: Hashable {
    case locked(MazeRoomLock)
    case unlocked(MazeRoomIdentifier)
    
    public var hashValue: Int {
        switch self {
        case .locked(let lock):
            return lock.lock.hashValue
        case .unlocked(let identifier):
            return identifier.id.hashValue
        }
    }
    
    public var id: String {
        switch self {
        case .locked(let lock):
            return lock.lock
        case .unlocked(let identifier):
            return identifier.id
        }
    }
    
    public static func ==(lhs: UnvisitedMazeRoom, rhs: UnvisitedMazeRoom) -> Bool {
        switch (lhs, rhs) {
        case let (.locked(lhs), .locked(rhs)):
            return lhs.lock == rhs.lock
        case let (.unlocked(lhs), .unlocked(rhs)):
            return lhs.id == rhs.id
        default:
            return false
        }
    }
}

public struct MazeRoom {
    
    public let id: String
    public let point: MazePoint
    public let tileURL: URL
    
    public var north: UnvisitedMazeRoom?
    public var east: UnvisitedMazeRoom?
    public var south: UnvisitedMazeRoom?
    public var west: UnvisitedMazeRoom?
    
    public var adjacentRooms: [UnvisitedMazeRoom] {
        return [north, east, south, west].flatMap({ $0 })
    }
    
    public init(id: String, point: MazePoint, tileURL: URL, north: UnvisitedMazeRoom? = nil, east: UnvisitedMazeRoom? = nil, south: UnvisitedMazeRoom? = nil, west: UnvisitedMazeRoom? = nil) {
        self.id = id
        self.point = point
        self.tileURL = tileURL
        self.north = north
        self.east = east
        self.south = south
        self.west = west
    }
}
