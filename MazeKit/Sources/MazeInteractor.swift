import Foundation
import os.log

public final class MazeInteractor: MazeInteractorProtocol {
    
    private static let log = OSLog(subsystem: String(describing: MazeInteractor.self), category: "Interactor")
    
    private let mazeRepository: MazeRepositoryProtocol
    
    private lazy var workQueue: DispatchQueue = DispatchQueue(label: "MazeInteractor.workQueue")
    private lazy var operationQueue = OperationQueue()
    
    private var unknownRooms = Array<UnvisitedMazeRoom>()
    private var knownRooms = Set<UnvisitedMazeRoom>()
    
    private var completion: MazeInteractorProtocol.CompletionHandler? = nil
    
    public init(mazeRepository: MazeRepositoryProtocol) {
        self.mazeRepository = mazeRepository
    }
    
    public func generate(fromPoint point: MazePoint, update: @escaping MazeInteractorProtocol.UpdateHandler, completion: @escaping MazeInteractorProtocol.CompletionHandler) {
        os_log("starting generation", log: MazeInteractor.log)
        
        self.completion = completion
        
        unknownRooms.removeAll()
        knownRooms.removeAll()
        
        mazeRepository.fetchStartingRoomIdentifier(pinnedTo: point) { [weak self] result in
            switch result {
            case .success(let identifier):
                self?.knownRooms.insert(.unlocked(identifier))
                self?.visit(unvisistedMazeRoom:.unlocked(identifier), update: update) { result in
                    os_log("finished generation", log: MazeInteractor.log)
                    completion(result)
                }
                
            case .failure(let error):
                os_log("failed generation", log: MazeInteractor.log)
                completion(.failed(error))
            }
        }
    }
    
    public func cancel() {
        os_log("cancel", log: MazeInteractor.log)
        operationQueue.cancelAllOperations()
        
        workQueue.async {
            self.completion?(.cancelled)
        }
    }

    private func visit(unvisistedMazeRoom unvisited: UnvisitedMazeRoom, update: @escaping MazeInteractorProtocol.UpdateHandler, completion: @escaping MazeInteractorProtocol.CompletionHandler) {
        let operation = AsyncBlockOperation { [weak self] operation in
            os_log("starting visit", log: MazeInteractor.log)
            self?.unlock(unvisitedMazeRoom: unvisited) { identifier in
                self?.mazeRepository.fetchRoom(withIdentifier: identifier) { result in
                    self?.workQueue.async {
                        defer {
                            operation.finish()
                        }
                        
                        guard let _self = self else {
                            os_log("no self", log: MazeInteractor.log)
                            return
                        }
                        
                        guard !operation.isCancelled else {
                            os_log("cancelled", log: MazeInteractor.log)
                            return
                        }
                        
                        switch result {
                        case .success(let room):
                            update((point: room.point, tileURL: room.tileURL))
                            os_log("updated", log: MazeInteractor.log)
                            
                            let adjacentUnknownRooms = room.adjacentRooms.filter({ !_self.knownRooms.contains($0) })
                            _self.unknownRooms.append(contentsOf: adjacentUnknownRooms)
                            _self.knownRooms.formUnion(adjacentUnknownRooms)
                            
                            if _self.operationQueue.operationCount == 1 && _self.unknownRooms.isEmpty {
                                os_log("no unknown", log: MazeInteractor.log)
                                completion(.finished)
                            }
                            else if _self.operationQueue.operationCount == 1 && !_self.unknownRooms.isEmpty {
                                for unvisited in _self.unknownRooms {
                                    guard !operation.isCancelled else {
                                        os_log("cancelled for each", log: MazeInteractor.log)
                                        return
                                    }
                                    _self.visit(unvisistedMazeRoom: unvisited, update: update, completion: completion)
                                }
                                _self.unknownRooms.removeAll(keepingCapacity: true)
                            }
                            
                        case .failure(let error):
                            os_log("failed fetch", log: MazeInteractor.log)
                            _self.operationQueue.cancelAllOperations()
                            completion(.failed(error))
                        }
                    }
                }
            }
        }
        
        operationQueue.addOperation(operation)
    }
    
    private func unlock(unvisitedMazeRoom: UnvisitedMazeRoom, completion: @escaping (MazeRoomIdentifier) -> Void) {
        switch unvisitedMazeRoom {
        case .locked(let lock):
            mazeRepository.unlockRoomIdentifier(withLock: lock, completion: completion)
        case .unlocked(let identifier):
            completion(identifier)
        }
    }
    
    private func unlock(unvisitedMazeRoom: UnvisitedMazeRoom) -> MazeRoomIdentifier {
        switch unvisitedMazeRoom {
        case .locked(let lock):
            return mazeRepository.unlockRoomIdentifier(withLock: lock)
        case .unlocked(let identifier):
            return identifier
        }
    }
    
    deinit {
        os_log("deinit", log: MazeInteractor.log)
    }
}
