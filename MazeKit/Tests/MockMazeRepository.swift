import Foundation

class MockMazeRepository: MazeRepositoryProtocol {
    
    private enum MockError: Error {
        case unknownIdentifier
    }
    
    private var _fetched = [String]()
    var fetched: [String] {
        return accessQueue.sync {
            return _fetched
        }
    }
    
    private var _duplicates = Set<String>()
    var duplicates: Set<String> {
        return accessQueue.sync {
            return _duplicates
        }
    }
    
    private var fetchDelayInSeconds: Double {
        return Double(arc4random_uniform(10)) / 100
    }
    
    private var unclokDelayInSeconds: Double {
        return Double(arc4random_uniform(3)) / 10
    }
    
    private let accessQueue = DispatchQueue(label: "TestMazeRepository.accessQueue", attributes: .concurrent)
    private let url = URL(string: "https://therealbnut.github.io/takehome-maze-tileset/images/tileset-0101.jpg")!
    
    func fetchStartingRoomIdentifier(pinnedTo point: MazePoint, completion: @escaping (Result<MazeRoomIdentifier>) -> Void) {
        completion(.success(MazeRoomIdentifier(id: "0", point: point)))
    }
    
    func fetchRoom(withIdentifier identifier: MazeRoomIdentifier, completion: @escaping (Result<MazeRoom>) -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now() + fetchDelayInSeconds) {
            self.register(id: identifier.id)
            
            guard let room = self.makeMazeRoom(identifier: identifier) else {
                completion(.failure(MockError.unknownIdentifier))
                return
            }
            completion(.success(room))
        }
    }
    
    func unlockRoomIdentifier(withLock lock: MazeRoomLock, completion: @escaping (MazeRoomIdentifier) -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now() + unclokDelayInSeconds) {
            completion(MazeRoomIdentifier(id: lock.lock, point: lock.point))
        }
    }
    
    func unlockRoomIdentifier(withLock lock: MazeRoomLock) -> MazeRoomIdentifier {
        usleep(UInt32(unclokDelayInSeconds * 1_000_000))
        return MazeRoomIdentifier(id: lock.lock, point: lock.point)
    }
    
    func cancel() {
        fatalError("Not supported")
    }
    
    private func register(id: String) {
        accessQueue.async(flags: .barrier) {
            if self._fetched.contains(id) {
                self._duplicates.insert(id)
            }
            self._fetched.append(id)
        }
    }
    
    /*
     * 3-5-7
     * 1 8 6
     * 0-2-4
     */
    private func makeMazeRoom(identifier: MazeRoomIdentifier) -> MazeRoom? {
        switch identifier.id {
        case "0":
            let north = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "1", point: identifier.point.north))
            let east = UnvisitedMazeRoom.locked(MazeRoomLock(lock: "2L", point: identifier.point.east))
            return MazeRoom(id: "0", point: identifier.point, tileURL: url, north: north, east: east)
            
        case "1":
            let north = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "3", point: identifier.point.north))
            let east = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "8", point: identifier.point.east))
            return MazeRoom(id: "1", point: identifier.point, tileURL: url, north: north, east: east)
            
        case "2L":
            let north = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "8", point: identifier.point.north))
            let east = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "4", point: identifier.point.east))
            return MazeRoom(id: "2L", point: identifier.point, tileURL: url, north: north, east: east)
            
        case "3":
            let east = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "5", point: identifier.point.east))
            let south = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "1", point: identifier.point.south))
            return MazeRoom(id: "3", point: identifier.point, tileURL: url, east: east, south: south)
            
        case "4":
            let north = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "6", point: identifier.point.north))
            let west = UnvisitedMazeRoom.locked(MazeRoomLock(lock: "2L", point: identifier.point.west))
            return MazeRoom(id: "4", point: identifier.point, tileURL: url, north: north, west: west)
            
        case "5":
            let east = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "7", point: identifier.point.east))
            let south = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "8", point: identifier.point.south))
            return MazeRoom(id: "5", point: identifier.point, tileURL: url, east: east, south: south)
            
        case "6":
            let north = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "7", point: identifier.point.north))
            let west = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "8", point: identifier.point.west))
            return MazeRoom(id: "6", point: identifier.point, tileURL: url, north: north, west: west)
            
        case "7":
            let south = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "6", point: identifier.point.south))
            let west = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "5", point: identifier.point.west))
            return MazeRoom(id: "7", point: identifier.point, tileURL: url, south: south, west: west)
            
        case "8":
            let north = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "5", point: identifier.point.north))
            let east = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "6", point: identifier.point.east))
            let south = UnvisitedMazeRoom.locked(MazeRoomLock(lock: "2L", point: identifier.point.south))
            let west = UnvisitedMazeRoom.unlocked(MazeRoomIdentifier(id: "1", point: identifier.point.west))
            return MazeRoom(id: "8", point: identifier.point, tileURL: url, north: north, east: east, south: south, west: west)
            
        default:
            return nil
        }
    }
}
