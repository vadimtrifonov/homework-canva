import XCTest
@testable import MazeKit

class MazeInteractorTests: XCTestCase {
    
    private var mazeRepository: MockMazeRepository!
    private var mazeInteractor: MazeInteractor!
    
    override func setUp() {
        super.setUp()
        mazeRepository = MockMazeRepository()
        mazeInteractor = MazeInteractor(mazeRepository: mazeRepository)
    }
    
    func testGeneration() {
        let textExpectation = expectation(description: "Generation should take less than 1 second")
        
        mazeInteractor.generate(fromPoint: MazePoint(x: 0, y: 0), update: { _ in }) { result in    
            XCTAssertEqual(self.mazeRepository.duplicates.count, 0)
            print(self.mazeRepository.fetched)
//            XCTAssertEqual(self.mazeRepository.fetched, ["0", "1", "2L", "3", "8", "4", "5", "6", "7"])
            textExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1, handler: nil)
    }
}
